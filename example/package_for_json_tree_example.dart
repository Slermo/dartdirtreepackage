import 'package:package_for_json_tree/package_for_json_tree.dart';
import 'dart:io';
import 'dart:isolate';
void main(List<String> args) async {
  String path= "";
  if(args.isNotEmpty){
  path = args.first;
  }

  FolderObserver observer = FolderObserver();
  try {
    await observer.changeMainFolder(path);
    await observer.observeFolder();
    print(observer.printElementsFromFolder());
  } catch (e) {
    print(e);
  }

}