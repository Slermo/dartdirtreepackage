import 'dart:async';
import 'dart:io';
import 'object_date_base.dart';

class FolderObserver {
  late Directory _curDir;
  late String _path;
  late Objectbox box;
  StreamSubscription? _stream;

  FolderObserver() {
    box = Objectbox();
    _delete();
  }

  /// Изменение корневой папки
  changeMainFolder(String path) async {
    _delete();
    _path = path;
    if (!(await _isDirExists(_path))) {
      return;
    }
    _curDir = Directory(path);
    box.initBox(path);
  }

  /// Начать отслеживание папки
  Future<void> observeFolder() async {
    if (!(await _isDirExists(_path))) {
      _stream?.cancel;
      _stream == null;
      return;
    }
    await _dirContents();
    _changesObserve();
  }

  /// Вернуть json объект с элементами папки
  String printElementsFromFolder() {
    return box.getFromBox();
  }

  /// Удаление содержимого бд
  Future<void> _delete() async {
    box.removeBox();
  }

  /// Подгрузка всех элементов из папки
  Future<void> _dirContents() async {
    var lister = _curDir.list(recursive: true);
    await for (var element in lister) {
      //element.statSync().type;
      //FileSystemEntityType.
      if (element.runtimeType.toString() == "_Directory") {
        box.addToBox(FileOrFolder.folder, element.path);
      } else if (element.runtimeType.toString() == "_File") {
        box.addToBox(FileOrFolder.file, element.path);
      }
    }
  }

  /// Ожидание изменений в папке
  _changesObserve() {
    _stream?.cancel;
    _stream == null;
    _stream = _curDir.watch(recursive: true).listen((event) {
      switch (event.type) {
        //Добавление
        case 1:
          {
            print(event.type);
            if (event.runtimeType.toString() == "_Directory") {
              box.addToBox(FileOrFolder.folder, event.path);
            } else if (event.runtimeType.toString() == "_File") {
              box.addToBox(FileOrFolder.file, event.path);
            }
            print("Added");
            printElementsFromFolder();
            break;
          }
        //Удаление
        case 4:
          {
            if (event.runtimeType.toString() == "_Directory") {
              box.removeFromBox(FileOrFolder.folder, event.path);
            } else if (event.runtimeType.toString() == "_File") {
              box.removeFromBox(FileOrFolder.file, event.path);
            }
            print("Deleted");
            printElementsFromFolder();
            break;
          }
      }
    });
  }

  /// Проверка на корректность введенных данных
  Future<bool> _isDirExists(String path) async {
    final checkPathExistence = await Directory(path).exists();
    if (!checkPathExistence) {
      throw ("Param PATH is wrong. Enter enother PATH with func changeMainFolder");
    }
    return checkPathExistence;
  }
}
