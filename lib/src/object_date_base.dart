import 'dart:convert';
import 'dart:io';
import 'objectbox.g.dart';

enum FileOrFolder { file, folder }

@Entity()
class Folder {
  @Id()
  int id = 0;

  @Index()
  String? path;

  @Backlink('upperFolder')
  final folder = ToMany<Folder>();
  @Backlink('upperFolder')
  final file = ToMany<File>();

  final upperFolder = ToOne<Folder>();
  Folder({required this.path});
}

@Entity()
class File {
  @Id()
  int id = 0;

  @Index()
  String? path;

  final upperFolder = ToOne<Folder>();
  File({required this.path});
}

//Для работы с бд ObjectBox
class Objectbox {
  late Store store;
  late Folder folder;
  //Содержит папки
  late Box<Folder> treeFolderBox;
  //Содержит файлы
  late Box<File> treeFileBox;
  Objectbox() {
    store = openStore();
    treeFolderBox = store.box<Folder>();
    treeFileBox = store.box<File>();
  }
  ///Создание корневой папки
  initBox(String curpath) {
    folder = Folder(path: curpath);
    treeFolderBox.put(folder);
  }

  ///Добавление элемента в базу данных
  Future<void> addToBox(FileOrFolder type, String curpath) async {
    var parentPath =
        curpath.substring(0, curpath.lastIndexOf(Platform.pathSeparator));

    //Поиск родительской папки
    final query = treeFolderBox
        .query(Folder_.path.contains(parentPath))
        .build()
        .findFirst();

    //Добавление в коробку файлов или папок
    switch (type) {
      case FileOrFolder.file:
        {
          final newitem = File(path: curpath);

          if (query != null) {
            newitem.upperFolder.target = treeFolderBox.get(query.id);
            treeFileBox.put(newitem);
          }
          break;
        }
      case FileOrFolder.folder:
        {
          final newitem = Folder(path: curpath);

          if (query != null) {
            newitem.upperFolder.target = treeFolderBox.get(query.id);
            treeFolderBox.put(newitem);
          }
          break;
        }
    }
  }

  ///Получение всех элементов коробки
  String getFromBox() {
    final folders = treeFolderBox.getAll();
    final files = treeFileBox.getAll();
    var finalJson = <Map>[];
    for (var element in files) {
      var file = {"Type": "File", "Path":element.path, "Parent":element.upperFolder.target?.path};
      finalJson.add(file);
    }
    for (var element in folders) {
      var file = {"Type": "Folder", "Path":element.path, "Parent":element.upperFolder.target?.path};
      finalJson.add(file);
    }

    var json =jsonEncode(finalJson);
    return json;
  }
  ///Удаление элемента бд
  removeFromBox(FileOrFolder type, String curpath) {
    //ищем файлы, которые включают такой путь
    final query = treeFolderBox.query(Folder_.path.contains(curpath)).build();
    final queryFiles = treeFileBox.query(File_.path.contains(curpath)).build();
    switch (type) {
      case FileOrFolder.file:
        {
          treeFileBox.remove(query.findFirst()!.id);
          break;
        }
      case FileOrFolder.folder:
        {
          //удаляем папки, потом файлы
          for (var element in query.find()) {
            treeFolderBox.remove(element.id);
          }
          for (var element in queryFiles.find()) {
            treeFileBox.remove(element.id);
          }
          break;
        }
    }
  }
  ///Удаление всего из бд
  removeBox() {
    treeFolderBox.removeAll();
    treeFileBox.removeAll();
  }
}
